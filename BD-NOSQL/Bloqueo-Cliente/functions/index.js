const functions = require("firebase-functions");

const admin = require("firebase-admin");

admin.initializeApp()
const db = admin.firestore();

exports.onUserCreate = functions.firestore

    .document("contrato/{userId}")
    .onCreate(async(snap, context) => {
        const datos = snap.data()
        const cliente = await db
            .collection("cliente")
            .where("id_cliente", "==", datos.id_cliente);
        const item = await cliente.get();
        let estado = true;
        item.forEach(
            (querySnapshot) =>
            estado =
            querySnapshot.data().estado_cuenta
        );

        if (!estado) {
            await db.collection("contrato")
                .doc(snap.id).delete();
            console.log("No se ha creado el contrato, tiene deuda");
        }

    });