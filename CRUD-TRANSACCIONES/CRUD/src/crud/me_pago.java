/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud;

import java.sql.*;
import java.text.SimpleDateFormat;
import javax.swing.JOptionPane;

/**
 *
 * @author leopo
 */
public class me_pago {
    
    Connection db = null;
    Statement st = null;
    ResultSet rs = null;
    PreparedStatement pst = null;
    CallableStatement cs;
    
    
    
    //Conexion a base de datos
    public void conecciondb(){
        try{
            db = DriverManager.getConnection("jdbc:postgresql://localhost:5432/aseguradora_salud_2","postgres","P140618");
        } catch(SQLException e){
            System.out.println("Ocurrio un error: " + e.getMessage());
        }
    }
    
    public void pago(String a1, String a2, String a3){
        
        conecciondb(); //Se conecta a la base de datos
        
        
        try {
            
            
            cs = db.prepareCall("call cuenta_detalle(?,?,?)");
            
            SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        
            String fech1 = a1;

            java.util.Date nfecha = formato.parse(fech1);

            java.sql.Date fecha1 = new java.sql.Date(nfecha.getTime());
            
            cs.setDate(1, fecha1);
            cs.setDouble(2, Double.parseDouble(a2));
            cs.setInt(3, Integer.parseInt(a3));
            
            cs.execute();
            
             JOptionPane.showMessageDialog(null,"Se realizó la transacción con éxito!");
            
            cs.close();
            db.close();
            
        } catch (Exception e){
            
            JOptionPane.showMessageDialog(null," Error en la transacción " + e.getMessage(), " error ", JOptionPane.ERROR_MESSAGE);
            
        }
          
        
    }
    
}
