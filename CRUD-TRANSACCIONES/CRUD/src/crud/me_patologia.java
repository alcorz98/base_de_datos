/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import javax.swing.JOptionPane;

/**
 *
 * @author leopo
 */
public class me_patologia {
    
    Connection db = null;
    Statement st = null;
    ResultSet rs = null;
    PreparedStatement pst = null;
    CallableStatement cs;
    
    
    
    //Conexion a base de datos
    public void conecciondb(){
        try{
            db = DriverManager.getConnection("jdbc:postgresql://localhost:5432/aseguradora_salud_2","postgres","P140618");
        } catch(SQLException e){
            System.out.println("Ocurrio un error: " + e.getMessage());
        }
    }
    
    public void patologia(String a1, String a2){
        
        conecciondb(); //Se conecta a la base de datos
        
        
        try {
            
            
            cs = db.prepareCall("call patologia_cliente(?,?)");
            
            cs.setInt(1, Integer.parseInt(a1));
            cs.setInt(2, Integer.parseInt(a2));
            
            cs.execute();
            
             JOptionPane.showMessageDialog(null,"Se realizó la transacción con éxito!");
            
            cs.close();
            db.close();
            
        } catch (Exception e){
            
            JOptionPane.showMessageDialog(null," Error en la transacción " + e.getMessage(), " error ", JOptionPane.ERROR_MESSAGE);
            
        }
          
        
    }
    
}
