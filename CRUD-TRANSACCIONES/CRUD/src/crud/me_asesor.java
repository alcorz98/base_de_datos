/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud;

import java.sql.*;
import javax.swing.JOptionPane;

/**
 *
 * @author leopo
 */
public class me_asesor {
    
    Connection db = null;
    Statement st = null;
    ResultSet rs = null;
    PreparedStatement pst = null;
    
    String nom, cedu, tel, corr, dir;
    String id;
    int id2;
    String sql;
    Boolean a = true;
    
    //Conexion a base de datos
    public void conecciondb(){
        try{
            db = DriverManager.getConnection("jdbc:postgresql://localhost:5432/aseguradora_salud_2","postgres","P140618");
        } catch(SQLException e){
            System.out.println("Ocurrio un error: " + e.getMessage());
        }
    }
    
    //Ingreso de datos
    public void inserta_asesor() throws SQLException{
        
        if(a == true){
            
            conecciondb(); //Se conecta a la base de datos
            
            id = asesor.a1.getText(); // Ingreso código
            nom = asesor.a2.getText(); // Ingreso nombre
            cedu = asesor.a3.getText(); // Ingreso cédula
            tel = asesor.a4.getText(); //Ingreso teléfono
            corr = asesor.a5.getText(); //Ingreso correo
            dir = asesor.a6.getText(); // Ingreso dirección
            
            id2 = Integer.parseInt(id);
            
            sql = "insert into asesor (id_asesor,nombre_asesor,cedula_asesor,telefono_asesor,correo_asesor,direccion_asesor) values (?,?,?,?,?,?)"; //Ingreso de sentencia sql
            pst = db.prepareStatement(sql);
            
            pst.setInt(1, id2);
            pst.setString(2, nom);
            pst.setString(3, cedu);
            pst.setString(4, tel);
            pst.setString(5, corr);
            pst.setString(6, dir);
            
            pst.executeUpdate();

            JOptionPane.showMessageDialog(null, "Se ha guardado exitosamente!");
            
        }
    }
    
    public void actuali_asesor() throws SQLException {
        
        if (a == true) {
            
            conecciondb();
            
            nom = asesor.a2.getText(); //Ingreso Nombre
            cedu = asesor.a3.getText(); // Ingreso cédula
            tel = asesor.a4.getText(); //Ingreso teléfono
            corr = asesor.a5.getText(); //Ingreso correo
            dir = asesor.a6.getText(); // Ingreso dirección
            
            sql = "update asesor set nombre_asesor=?,cedula_asesor=?, telefono_asesor=?, correo_asesor=?, direccion_asesor=? where id_asesor = '"+id+"'"; //Sentencia sql
            
            pst = db.prepareStatement(sql);

            pst.setString(1, nom);
            pst.setString(2, cedu);
            pst.setString(3, tel);
            pst.setString(4, corr);
            pst.setString(5, dir);

            pst.executeUpdate();

            JOptionPane.showMessageDialog(null, "Se ha actualizado exitosamente!");
        
        }
        
    }
    
    public void consulta_asesor() throws SQLException {
        
        conecciondb();
        
        st = db.createStatement();
        
        id = asesor.a1.getText();
        
        rs = st.executeQuery("select * from asesor where id_asesor = '"+id+"'"); 
        
        if (rs.next()){
            
            a = true;
            
            asesor.a2.setText(rs.getString(2));
            asesor.a3.setText(rs.getString(3));
            asesor.a4.setText(rs.getString(4));
            asesor.a5.setText(rs.getString(5));
            asesor.a6.setText(rs.getString(6));
        }
        else {
            
            JOptionPane.showMessageDialog(null, "No existe registro");
            
            a = false;
        }
    }
    
    public void elimina_asesor() throws SQLException {
        
        try{
            
            if(a == true){
                
                conecciondb();
                
                int resp = JOptionPane.showConfirmDialog(null, "¿Desea eliminar este asesor?", "ALERTA", JOptionPane.YES_NO_OPTION);
                
                if(resp != 1){
                    
                    st.execute("delete from asesor where id_asesor = '"+id+"'");
                    
                    JOptionPane.showMessageDialog(null,"Se ha eliminado con éxito, ya que no tiene relacion en la tabla!");
                    
                }
                
            }
        } catch (SQLException e){
            
            JOptionPane.showMessageDialog(null,"No se puede eliminar, ya que tiene relacion en la tabla!");
            
        }
        
    }
    
    public void limpia(){
        
        asesor.a1.setText("");
        asesor.a2.setText(""); 
        asesor.a3.setText(""); 
        asesor.a4.setText(""); 
        asesor.a5.setText(""); 
        asesor.a6.setText(""); 
        
    }
    
}
