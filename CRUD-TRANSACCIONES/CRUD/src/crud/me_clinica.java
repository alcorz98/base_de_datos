/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud;

import java.sql.*;
import javax.swing.JOptionPane;

/**
 *
 * @author Paul Quiñonez
 */
public class me_clinica {
    
    Connection db = null;
    Statement st = null;
    ResultSet rs = null;
    PreparedStatement pst = null;
    
    String nom, tel, dir;
    String id;
    int id2;
    String sql;
    Boolean a = true;
    
    //Conexion a base de datos
    public void conecciondb(){
        try{
            db = DriverManager.getConnection("jdbc:postgresql://localhost:5432/aseguradora_salud_2","postgres","P140618");
        } catch(SQLException e){
            System.out.println("Ocurrio un error: " + e.getMessage());
        }
    }
    
    //Ingreso de datos
    public void inserta_clinica() throws SQLException{
        
        if(a == true){
            
            conecciondb(); //Se conecta a la base de datos
            
            id = clinica.a1.getText(); // Ingreso código
            nom = clinica.a2.getText(); // Ingreso nombre
            tel = clinica.a4.getText(); //Ingreso teléfono
            dir = clinica.a6.getText(); // Ingreso dirección
            
            id2 = Integer.parseInt(id);
            
            sql = "insert into clinica (id_clinica,nombre_clinica,telefono_clinica,direccion_clinica) values (?,?,?,?)"; //Ingreso de sentencia sql
            pst = db.prepareStatement(sql);
            
            pst.setInt(1, id2);
            pst.setString(2, nom);
            pst.setString(3, tel);
            pst.setString(4, dir);
            
            pst.executeUpdate();

            JOptionPane.showMessageDialog(null, "Se ha guardado exitosamente!");
            
        }
    }
    
    public void actuali_clinica() throws SQLException {
        
        if (a == true) {
            
            conecciondb();
            
            nom = clinica.a2.getText(); //Ingreso Nombre
            tel = clinica.a4.getText(); //Ingreso teléfono
            dir = clinica.a6.getText(); // Ingreso dirección
            
            sql = "update clinica set nombre_clinica=?,telefono_clinica=?, direccion_clinica=? where id_clinica = '"+id+"'"; //Sentencia sql
            
            pst = db.prepareStatement(sql);

            pst.setString(1, nom);
            pst.setString(2, tel);
            pst.setString(3, dir);

            pst.executeUpdate();

            JOptionPane.showMessageDialog(null, "Se ha actualizado exitosamente!");
        
        }
        
    }
    
    public void consulta_clinica() throws SQLException {
        
        conecciondb();
        
        st = db.createStatement();
        
        id = clinica.a1.getText();
        
        rs = st.executeQuery("select * from clinica where id_clinica = '"+id+"'"); 
        
        if (rs.next()){
            
            a = true;
            
            clinica.a2.setText(rs.getString(2));
            clinica.a4.setText(rs.getString(3));
            clinica.a6.setText(rs.getString(4));
        }
        else {
            
            JOptionPane.showMessageDialog(null, "No existe registro");
            
            a = false;
        }
    }
    
    public void elimina_clinica() throws SQLException {
        
        try{
            
            if(a == true){
                
                conecciondb();
                
                int resp = JOptionPane.showConfirmDialog(null, "¿Desea eliminar esta clínica?", "ALERTA", JOptionPane.YES_NO_OPTION);
                
                if(resp != 1){
                    
                    st.execute("delete from clinica where id_clinica = '"+id+"'");
                    
                    JOptionPane.showMessageDialog(null,"Se ha eliminado con éxito, ya que no tiene relacion en la tabla!");
                    
                }
                
            }
        } catch (SQLException e){
            
            JOptionPane.showMessageDialog(null,"No se puede eliminar, ya que tiene relacion en la tabla!");
            
        }
        
    }
    
    public void limpia(){
        
        clinica.a1.setText("");
        clinica.a2.setText("");  
        clinica.a4.setText(""); 
        clinica.a6.setText(""); 
        
    }
    
}
