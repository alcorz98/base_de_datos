/*==============================================================*/
/* DBMS name:      PostgreSQL 9.x                               */
/* Created on:     13/11/2021 03:27:55 p. m.                    */
/*==============================================================*/

/*==============================================================*/
/* Table: ASESOR                                                */
/*==============================================================*/
create table ASESOR (
   ID_ASESOR            INT4                 not null,
   NOMBRE_ASESOR        VARCHAR(45)          null,
   CEDULA_ASESOR        VARCHAR(11)          null,
   TELEFONO_ASESOR      VARCHAR(45)          null,
   CORREO_ASESOR        VARCHAR(45)          null,
   DIRECCION_ASESOR     VARCHAR(45)          null,
   constraint PK_ASESOR primary key (ID_ASESOR)
);

/*==============================================================*/
/* Index: ASESOR_PK                                             */
/*==============================================================*/
create unique index ASESOR_PK on ASESOR (
ID_ASESOR
);

/*==============================================================*/
/* Table: ATENCION                                              */
/*==============================================================*/
create table ATENCION (
   ID_ATENCION          INT4                 not null,
   ID_CONTRATO          INT4                 null,
   ID_CLINICA           INT4                 null,
   MOTIVO_ATENCION      VARCHAR(45)          null,
   FECHA_ATENCION       DATE                 null,
   PRECIO_ATENCION      DECIMAL              null,
   constraint PK_ATENCION primary key (ID_ATENCION)
);

/*==============================================================*/
/* Index: ATENCION_PK                                           */
/*==============================================================*/
create unique index ATENCION_PK on ATENCION (
ID_ATENCION
);

/*==============================================================*/
/* Index: RELATIONSHIP_9_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_9_FK on ATENCION (
ID_CONTRATO
);

/*==============================================================*/
/* Index: RELATIONSHIP_10_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_10_FK on ATENCION (
ID_CLINICA
);

/*==============================================================*/
/* Table: CLIENTE                                               */
/*==============================================================*/
create table CLIENTE (
   ID_CLIENTE           INT4                 not null,
   ID_ASESOR            INT4                 null,
   NOMBRE_CLIENTE       VARCHAR(45)          null,
   CEDULA_CLIENTE       VARCHAR(11)          null,
   TELEFONO_CLIENTE     VARCHAR(11)          null,
   CORREO_CLIENTE       VARCHAR(45)          null,
   DIRECCION_CLIENTE    VARCHAR(45)          null,
   TIPO_SANGRE          VARCHAR(3)           null,
   ESTADO_CUENTA        BOOL                 null,
   constraint PK_CLIENTE primary key (ID_CLIENTE)
);

/*==============================================================*/
/* Index: CLIENTE_PK                                            */
/*==============================================================*/
create unique index CLIENTE_PK on CLIENTE (
ID_CLIENTE
);

/*==============================================================*/
/* Index: RELATIONSHIP_1_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_1_FK on CLIENTE (
ID_ASESOR
);

/*==============================================================*/
/* Table: CLINICA                                               */
/*==============================================================*/
create table CLINICA (
   ID_CLINICA           INT4                 not null,
   NOMBRE_CLINICA       VARCHAR(45)          null,
   TELEFONO_CLINICA     VARCHAR(11)          null,
   DIRECCION_CLINICA    VARCHAR(45)          null,
   constraint PK_CLINICA primary key (ID_CLINICA)
);

/*==============================================================*/
/* Index: CLINICA_PK                                            */
/*==============================================================*/
create unique index CLINICA_PK on CLINICA (
ID_CLINICA
);

/*==============================================================*/
/* Table: CONTRATO                                              */
/*==============================================================*/
create table CONTRATO (
   ID_CONTRATO          INT4                 not null,
   ID_CLIENTE           INT4                 null,
   ESTADO               VARCHAR(30)          null,
   FECHA_CONTRATO       DATE                 null,
   PRECIO_CONTRATO      DECIMAL              null,
   constraint PK_CONTRATO primary key (ID_CONTRATO)
);

/*==============================================================*/
/* Index: CONTRATO_PK                                           */
/*==============================================================*/
create unique index CONTRATO_PK on CONTRATO (
ID_CONTRATO
);

/*==============================================================*/
/* Index: RELATIONSHIP_2_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_2_FK on CONTRATO (
ID_CLIENTE
);

/*==============================================================*/
/* Table: DETALLE                                               */
/*==============================================================*/
create table DETALLE (
   ID_DETALLE           INT4                 not null,
   ID_FACTURA           INT4                 null,
   ID_SERVICIO          INT4                 null,
   ID_POLIZA            INT4                 null,
   DESCRIPCION          VARCHAR(45)          null,
   PRECIO_DETALLE       DECIMAL              null,
   constraint PK_DETALLE primary key (ID_DETALLE)
);

/*==============================================================*/
/* Index: DETALLE_PK                                            */
/*==============================================================*/
create unique index DETALLE_PK on DETALLE (
ID_DETALLE
);

/*==============================================================*/
/* Index: RELATIONSHIP_6_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_6_FK on DETALLE (
ID_FACTURA
);

/*==============================================================*/
/* Index: RELATIONSHIP_7_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_7_FK on DETALLE (
ID_SERVICIO
);

/*==============================================================*/
/* Index: RELATIONSHIP_8_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_8_FK on DETALLE (
ID_POLIZA
);

/*==============================================================*/
/* Table: DOCTOR                                                */
/*==============================================================*/
create table DOCTOR (
   ID_DOCTOR            INT4                 not null,
   CEDULA_DOCTOR        VARCHAR(45)          null,
   NOMBRE_DOCTOR        VARCHAR(45)          null,
   TELEFONO_DOCTOR      VARCHAR(45)          null,
   DIRECCION_DOCTOR     VARCHAR(45)          null,
   CORREO_DOCTOR        VARCHAR(45)          null,
   constraint PK_DOCTOR primary key (ID_DOCTOR)
);

/*==============================================================*/
/* Index: DOCTOR_PK                                             */
/*==============================================================*/
create unique index DOCTOR_PK on DOCTOR (
ID_DOCTOR
);

/*==============================================================*/
/* Table: ESPECIALIDAD                                          */
/*==============================================================*/
create table ESPECIALIDAD (
   ID_ESPECIA           INT4                 not null,
   ID_DOCTOR            INT4                 null,
   ID_ATENCION          INT4                 null,
   TIPO_ESPECIA         VARCHAR(45)          null,
   constraint PK_ESPECIALIDAD primary key (ID_ESPECIA)
);

/*==============================================================*/
/* Index: ESPECIALIDAD_PK                                       */
/*==============================================================*/
create unique index ESPECIALIDAD_PK on ESPECIALIDAD (
ID_ESPECIA
);

/*==============================================================*/
/* Index: RELATIONSHIP_12_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_12_FK on ESPECIALIDAD (
ID_ATENCION
);

/*==============================================================*/
/* Index: RELATIONSHIP_14_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_14_FK on ESPECIALIDAD (
ID_DOCTOR
);

/*==============================================================*/
/* Table: FACTURA                                               */
/*==============================================================*/
create table FACTURA (
   ID_FACTURA           INT4                 not null,
   ID_CONTRATO          INT4                 null,
   FECHA_FACTURA        DATE                 null,
   constraint PK_FACTURA primary key (ID_FACTURA)
);

/*==============================================================*/
/* Index: FACTURA_PK                                            */
/*==============================================================*/
create unique index FACTURA_PK on FACTURA (
ID_FACTURA
);

/*==============================================================*/
/* Index: RELATIONSHIP_5_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_5_FK on FACTURA (
ID_CONTRATO
);

/*==============================================================*/
/* Table: PATOLOGIA                                             */
/*==============================================================*/
create table PATOLOGIA (
   ID_PATOLOGIA         INT4                 not null,
   ID_CLIENTE           INT4                 null,
   TIPO_PATOLOGIA       VARCHAR(45)          null,
   constraint PK_PATOLOGIA primary key (ID_PATOLOGIA)
);

/*==============================================================*/
/* Index: PATOLOGIA_PK                                          */
/*==============================================================*/
create unique index PATOLOGIA_PK on PATOLOGIA (
ID_PATOLOGIA
);

/*==============================================================*/
/* Index: RELATIONSHIP_13_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_13_FK on PATOLOGIA (
ID_CLIENTE
);

/*==============================================================*/
/* Table: POLIZA                                                */
/*==============================================================*/
create table POLIZA (
   ID_POLIZA            INT4                 not null,
   ID_CONTRATO          INT4                 null,
   PLAN                 VARCHAR(45)          null,
   constraint PK_POLIZA primary key (ID_POLIZA)
);

/*==============================================================*/
/* Index: POLIZA_PK                                             */
/*==============================================================*/
create unique index POLIZA_PK on POLIZA (
ID_POLIZA
);

/*==============================================================*/
/* Index: RELATIONSHIP_4_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_4_FK on POLIZA (
ID_CONTRATO
);

/*==============================================================*/
/* Table: SERVICIO                                              */
/*==============================================================*/
create table SERVICIO (
   ID_SERVICIO          INT4                 not null,
   ID_CONTRATO          INT4                 null,
   TIPO_SERVICIO        VARCHAR(45)          null,
   PRECIO_SERVICIO      DECIMAL              null,
   constraint PK_SERVICIO primary key (ID_SERVICIO)
);

/*==============================================================*/
/* Index: SERVICIO_PK                                           */
/*==============================================================*/
create unique index SERVICIO_PK on SERVICIO (
ID_SERVICIO
);

/*==============================================================*/
/* Index: RELATIONSHIP_3_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_3_FK on SERVICIO (
ID_CONTRATO
);

alter table ATENCION
   add constraint FK_ATENCION_RELATIONS_CLINICA foreign key (ID_CLINICA)
      references CLINICA (ID_CLINICA)
      on delete restrict on update restrict;

alter table ATENCION
   add constraint FK_ATENCION_RELATIONS_CONTRATO foreign key (ID_CONTRATO)
      references CONTRATO (ID_CONTRATO)
      on delete restrict on update restrict;

alter table CLIENTE
   add constraint FK_CLIENTE_RELATIONS_ASESOR foreign key (ID_ASESOR)
      references ASESOR (ID_ASESOR)
      on delete restrict on update restrict;

alter table CONTRATO
   add constraint FK_CONTRATO_RELATIONS_CLIENTE foreign key (ID_CLIENTE)
      references CLIENTE (ID_CLIENTE)
      on delete restrict on update restrict;

alter table DETALLE
   add constraint FK_DETALLE_RELATIONS_FACTURA foreign key (ID_FACTURA)
      references FACTURA (ID_FACTURA)
      on delete restrict on update restrict;

alter table DETALLE
   add constraint FK_DETALLE_RELATIONS_SERVICIO foreign key (ID_SERVICIO)
      references SERVICIO (ID_SERVICIO)
      on delete restrict on update restrict;

alter table DETALLE
   add constraint FK_DETALLE_RELATIONS_POLIZA foreign key (ID_POLIZA)
      references POLIZA (ID_POLIZA)
      on delete restrict on update restrict;

alter table ESPECIALIDAD
   add constraint FK_ESPECIAL_RELATIONS_ATENCION foreign key (ID_ATENCION)
      references ATENCION (ID_ATENCION)
      on delete restrict on update restrict;

alter table ESPECIALIDAD
   add constraint FK_ESPECIAL_RELATIONS_DOCTOR foreign key (ID_DOCTOR)
      references DOCTOR (ID_DOCTOR)
      on delete restrict on update restrict;

alter table FACTURA
   add constraint FK_FACTURA_RELATIONS_CONTRATO foreign key (ID_CONTRATO)
      references CONTRATO (ID_CONTRATO)
      on delete restrict on update restrict;

alter table PATOLOGIA
   add constraint FK_PATOLOGI_RELATIONS_CLIENTE foreign key (ID_CLIENTE)
      references CLIENTE (ID_CLIENTE)
      on delete restrict on update restrict;

alter table POLIZA
   add constraint FK_POLIZA_RELATIONS_CONTRATO foreign key (ID_CONTRATO)
      references CONTRATO (ID_CONTRATO)
      on delete restrict on update restrict;

alter table SERVICIO
   add constraint FK_SERVICIO_RELATIONS_CONTRATO foreign key (ID_CONTRATO)
      references CONTRATO (ID_CONTRATO)
      on delete restrict on update restrict;

